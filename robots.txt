User-agent: *
Disallow: /comments/feed
Disallow: /feed/$
Disallow: /*/feed/$
Disallow: /go/
Disallow: /*/feed/rss/$
Disallow: /*/trackback/$
Disallow: /*/*/feed/$
Disallow: /*/*/feed/rss/$
Disallow: /*/*/trackback/$
Disallow: /*/*/*/feed/$
Disallow: /*/*/*/feed/rss/$
Disallow: /*/*/*/trackback/$
Disallow: /trackback/
Disallow: /*.inc$
Disallow: */trackback/

User-agent: Mediapartners-Google
Allow: /

User-agent: Adsbot-Google
Allow: /

User-agent: Googlebot-Image
Allow: /

User-agent: Googlebot-Mobile
Allow: /

User-agent: ia_archiver
Disallow: /

Sitemap: https://crypto1337.net/sitemap.xml